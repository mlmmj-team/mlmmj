mlmmj for Debian
----------------

0. Contents
 1. Installation
  1.1. Postfix
  1.1.1. Recipient delimiter
  1.1.2. Using the system aliases
  1.1.3. Using virtual maps
  1.2. Exim
  1.3. Sendmail
  1.4. Qmail
 2. Creating a mailing list
  2.1. Using /usr/bin/mlmmj-make-ml
  2.2. Old version of /usr/bin/mlmmj-make-ml
 3. MLMMJ PHP Web Admin
  3.1. Setting-up Apache2

1. Installation

1.1. Postfix

1.1.1. Recipient delimiter

    Make sure that `recipient_delimiter' is set to `+'; run:

      dpkg-reconfigure -plow postfix

    if in doubt, or edit /etc/postfix/main.cf and restart postfix.

1.1.2. Using the system aliases

    Create mailing lists using the -a option to mlmmj-make-ml, then
    (as instructed) run "/usr/bin/newaliases".

    Don't worry about the cron job, there's already one in place for all
    the lists in /etc/cron.d/mlmmj.

    Note that you may need (or not) to:

      chown -R nobody:nogroup /var/spool/mlmmj/list

    depending on your postfix setup.

1.1.2. Using virtual maps

    If you don't want to use system aliases (for example if you need
    to do mass hosting of mailing lists), you can use a more complex
    setup: virtual_alias_maps and alias_maps in postfix, and custom
    path for each of your lists. It's a little bit more complex, but
    not so much.

    The following directives in postfix main.cf instead:

      virtual_alias_maps = hash:/etc/postfix/postfix_virtual_mlmmj
      alias_maps = hash:/etc/aliases, hash:/etc/postfix_aliases_mlmmj

    This will extend the system aliases.

    Now let's pretend we want to add a new mylist@example.com list.
    We create it with mlmmj-make-ml (without the -a option):

      (echo example.com; echo listmaster@example.com; echo N;) | /usr/bin/mlmmj-make-ml -L example.com_mylist -s /path/to/example.com/lists

    In the file /etc/postfix/postfix_virtual put:

      mylist@example.com example.com_mylist

    In the file postfix_aliases_mlmmj, put:

      example.com_mylist: "|/usr/bin/mlmmj-recieve -L /path/to/example.com/lists/example.com_mylist/"

    Don't forget to:

      postmap /etc/postfix/postfix_virtual_mlmmj
      postmap /etc/postfix_aliases_mlmmj
      /etc/init.d/postfix reload

    each time you add a new list.

1.2. Exim

    For Exim intergration it is recommended to follow the docs in:
    /usr/share/doc/mlmmj/README.exim4

    Rather than using a mlmmj user and group you can:

      chown -R Debian-exim:Debian-exim /var/spool/mlmmj/mlmmj-test

    It is also possible to use the /etc/aliases file as in "1.1 Postfix"
    part of this file and add the following to the "userforward:" and
    "localuser:" exim configuration:

      local_part_suffix = +*
      local_part_suffix_optional

    Running "/usr/bin/newaliases" is not required and the there's already
    a cron job in /etc/cron.d/mlmmj in place to handle all lists.

1.3. Sendmail

    For use with sendmail please refer to:
    /usr/share/doc/mlmmj/README.sendmail

1.4. Qmail

    For use with qmail please refer to:
    /usr/share/doc/mlmmj/README.qmail

2. Creating a mailing list

2.1. Using /usr/bin/mlmmj-make-ml

    The installed version of mlmmj-make-ml is an untouched version from
    the upstream release. Run "man mlmmj-make-ml" for details of usage.
    If the exim4 package is installed then it is not necessary to add
    anything to /etc/aliases if you have correctly intergrated mlmmj into
    the exim4 configuration. The only option recommended is "-L" to name
    the mailing list.
    
    Read /usr/share/doc/mlmmj/TUNABLES.gz to configure your mailing list by
    adding/editing files in /var/spool/mlmmj/[list name]/control/.
    
    Use /usr/bin/mlmmj-sub to subscribe people to your mailing list.

2.2. Old version of /usr/bin/mlmmj-make-ml

    You can find the old version of /usr/bin/mlmmj-make-ml in
    /usr/share/doc/mlmmj/ directory. This depends on the dctrl-tools
    package.

3. MLMMJ PHP Web Admin
3.1. Setting-up Apache2

    To use mlmmj-php-web-admin add something like this in one of your
    virtual host files (for example in /etc/apache2/sites-available/default):

        <Directory "/usr/share/mlmmj-php-web-admin">
                AllowOverride All
        </Directory>
        Alias /mlmmj-php-web-admin /usr/share/mlmmj-php-web-admin/htdocs

    The AllowOverride All is necessary for the default .htaccess to work.

3.2. Configuring MLMMJ Web Admin

    Set the path of your list in /etc/mlmmj-web-admin/config.php.

    Set a login and password as follow:

	htpasswd -c /etc/mlmmj-php-web-admin/htpasswd YOUR-USERNAME
